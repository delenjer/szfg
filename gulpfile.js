var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var cssnano = require('gulp-cssnano');
var rename = require("gulp-rename");
var del = require("del");
var imagemin = require('gulp-imagemin');
var imageminPngquant = require('imagemin-pngquant');
var autoprefixer = require('gulp-autoprefixer');
var uncss = require('gulp-uncss');
var useref = require('gulp-useref');



//Sass
gulp.task('sass', function () {
  return gulp.src('app/sass/**/*.scss')
    .pipe(sass())//вызов (плагина)
    .pipe(autoprefixer(['last 20 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
    .pipe(gulp.dest('app/css'))//результат работы
    .pipe(browserSync.reload({stream: true}));
});

//Uncss
// gulp.task('uncss', function () {
//     return gulp.src('app/libs/bootstrap/css/bootstrap.css')
//         .pipe(uncss({
//             html: ['app/index.html']
//         }))
//         .pipe(gulp.dest('app/css'));
// });

//Scripts plagins
gulp.task('scripts-plagins', function () {
  return gulp.src([
      'app/libs/jquery-3.0.0.min.js',
      'app/libs/jquery-migrate-1.4.1.min.js',
      'app/libs/components/jquery.mCustomScrollbar.js',
      'app/libs/components/jquery.formstyler.js'
  	])
  	.pipe(concat('libs.min.js'))   
  	.pipe(uglify())   
  	.pipe(gulp.dest('app/js'));   
});

//Useref
gulp.task('useref', function () {
    return gulp.src('app/index.html')
        .pipe(useref())
        .pipe(gulp.dest('dist'));
});

//Css libs
gulp.task('css-libs', ['sass'], function() {
    return gulp.src([
        'app/css/libs.css',
        'app/css/style.css'
      ])
        .pipe(cssnano())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('app/css'));
});

//Img
gulp.task('img', function() {
    return gulp.src('app/img/**/*')
    .pipe(imagemin({
    	interlaced: true,
    	progressive: true,
    	svgoPlugins: [{removeViewBox: true}],
    	une: [imageminPngquant()]
    }))
    .pipe(gulp.dest('dist/img'));
        
});

// Local server
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "app"
        },
        notify: false
    });
});

//Clean
gulp.task('clean', function(){
	return del.sync('dist');
});


//Watch
gulp.task('watch', ['browser-sync','css-libs','scripts-plagins','useref'], function () {
  gulp.watch('app/sass/**/*.scss', ['sass']);
  gulp.watch('app/*.html', browserSync.reload);
  gulp.watch('app/js/**/*.js', browserSync.reload);
});


//Build
gulp.task('build', ['clean', 'img', 'sass','scripts-plagins'], function(){
	var buildCss = gulp.src([
			'app/css/style.css',
			'app/css/libs.min.css'
		])
	.pipe(gulp.dest('dist/css'));

	var buildJs = gulp.src('app/js/**/*.js')
	.pipe(gulp.dest('dist/js'));

	var buildHtml = gulp.src('app/*.html')
	.pipe(gulp.dest('dist'));
});