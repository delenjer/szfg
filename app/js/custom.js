$(window).load(function(){
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		$('body').addClass('ios');
	} else{
		$('body').addClass('web');
	};
	$('body').removeClass('loaded'); 
});
/* viewport width */
function viewport(){
	var e = window, 
		a = 'inner';
	if ( !( 'innerWidth' in window ) )
	{
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
};
/* viewport width */
$(function(){
	/* placeholder*/	   
	$('input, textarea').each(function(){
 		var placeholder = $(this).attr('placeholder');
 		$(this).focus(function(){ $(this).attr('placeholder', '');});
 		$(this).focusout(function(){			 
 			$(this).attr('placeholder', placeholder);  			
 		});
 	});
	/* placeholder*/

	$('.button-nav').click(function(){
		$(this).toggleClass('active'), 
		$('.main-nav-list').slideToggle(); 
		return false;
	});
	
	/* components */

	//TAB
	$('.content-s>p.tabs-content_item:first').show();

    $('.tab_control>li').each(function(i){
        $(this).attr('data-tab', i);
    })
    $('.tabs-content_item').each(function(i){
        $(this).attr('data-tab', i);
    })
    $('.tab_control>li').click(function(){
	    $('.tab_control>li').removeClass('active');
	    $(this).addClass('active');
	    var thisTab = $(this).attr('data-tab');
	    $('.tabs-content_item').hide();
	    $('.tabs-content_item[data-tab='+thisTab+']').show();
 	});
    //---------------------------//\\------------------------------/
 	$('.content-help>p.content-help_item:first').show();

    $('.tab_help>li').each(function(i){
        $(this).attr('data-tab', i);
    })
    $('.content-help_item').each(function(i){
        $(this).attr('data-tab', i);
    })
    $('.tab_help>li').click(function(){
	    $('.tab_help>li').removeClass('active');
	    $(this).addClass('active');
	    var thisTab = $(this).attr('data-tab');
	    $('.content-help_item').hide();
	    $('.content-help_item[data-tab='+thisTab+']').show();
 	});

 	//---------------------------END------------------------------/

	//Scroll menu
	// $('.go_to').click( function(){
	// var scroll_el = $(this).attr('href'); 
 //        if ($(scroll_el).length != 0) { 
	//     $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 500);
 //        }
	//     return false;
 //    });


 	//MODAL WINDOW
 	$('.show-modal-box-small').on('click', function() {
  
	  $('.overlay').show();

	  $('.modal-box-small').show();

	});
	$('.hide-modal').on('click', function() {
	  $('.modal-box-small').hide();
	  $('.overlay').hide();
	});

	//-------------------------------------------------//
	$('.show-modal-box-big').on('click', function() {
  
	  $('.overlay').show();

	  $('.modal-box-big').show();

	});
	$('.hide-modal').on('click', function() {
	  $('.modal-box-big').hide();
	  $('.overlay').hide();
	});



	$('#multiscroll').multiscroll({
		
	});
	
	if($('.styled').length) {
		$('.styled').styler();
	};


	// if($('.fancybox').length) {
	// 	$('.fancybox').fancybox({
	// 		margin  : 10,
	// 		padding  : 10
	// 	});
	// };


	// if($('.slick-slider').length) {
	// 	$('.slick-slider').slick({
	// 		dots: true,
	// 		infinite: false,
	// 		speed: 300,
	// 		slidesToShow: 4,
	// 		slidesToScroll: 4,
	// 		responsive: [
	// 			{
	// 			  breakpoint: 1024,
	// 			  settings: {
	// 				slidesToShow: 3,
	// 				slidesToScroll: 3,
	// 				infinite: true,
	// 				dots: true
	// 			  }
	// 			},
	// 			{
	// 			  breakpoint: 600,
	// 			  settings: "unslick"
	// 			}				
	// 		]
	// 	});
	// };

	if($('.scroll').length) {
		$(".scroll").mCustomScrollbar({
			axis:"y",
			theme:"my-theme",
			autoExpandScrollbar:true,
			advanced:{autoExpandHorizontalScroll:true}
		});
	};
	
	
	
	/* components */
	

});

var handler = function(){
	
	var height_footer = $('footer').height();	
	var height_header = $('header').height();		
	//$('.content').css({'padding-bottom':height_footer+40, 'padding-top':height_header+40});
	
	
	var viewport_wid = viewport().width;
	var viewport_height = viewport().height;
	
	if (viewport_wid <= 991) {
		
	}
	
}
$(window).bind('load', handler);
$(window).bind('resize', handler);



