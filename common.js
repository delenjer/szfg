$(document).ready(function() {

    //Slider

//     $('.bxslider').bxSlider({
//       minSlides: 3,
//       maxSlides: 5,
//       slideWidth: 180,
//       slideMargin: 10,
//       slideZIndex: 100
//     });
// $('.bx-prev').html('<i class="ion-chevron-left"></i>');
// $('.bx-next').html('<i class="ion-chevron-right"></i>');

    $('.bxslider6').bxSlider({
          minSlides: 1,
          maxSlides: 1,
          //slideWidth: 180,
          slideMargin: 10,
          slideZIndex: 100
        });
    $('.bx-prev').html('<i class="ion-chevron-left"></i>');
    $('.bx-next').html('<i class="ion-chevron-right"></i>');

    //Show/Hide menu
	$('.menu-toggle').on('click', function() {
		$('.menu-box_menu').toggleClass('toggle-menu-box_menu');
	});

    $('.slider5').bxSlider({
        slideWidth: 300,
        minSlides: 7,
        maxSlides: 7,
        moveSlides: 3,
        slideMargin: 0,
        controls: false,
        slideWidth: 200
  });


	//Scroll to
	$('.go_to').click( function(){
	var scroll_el = $(this).attr('href'); 
        if ($(scroll_el).length != 0) { 
	    $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 2000);
        }
	    return false;
    });

	
    //Bad code
    $('.form-outer').css({
        'width': '280px',
        'margin': '0 auto',
        'padding': '10px 0 5px 0',
        'text-align': 'center',
        'color': '#fff'

    });
    
    $('.sp-field input').css({
        'width': '100%',
        'height': '41px',
        'margin-bottom': '10px',
        'padding-left': '10px',
        'color': '#34184f',
        'font-size': '16px',
        'font-style': 'italic'
    });

    $('.input').css({
        'border-radius': '0'
    });

    $('.sp-button-container').css({
        'background-color': '#e2db1b',
        'border-radius': '27px',
        'margin': '23px auto'
    });

    $('.sp-button').css({
        'width': '100%',
        'height':'38px',
        'border-radius': '27px',
        'font-family': "MuseoSansBlack",
        'font-size': '18px',
        'font-weight': '700',
        'color': '#000',
        'border': 'none',
        'outline': 'none',
        'background-color':'inherit'
        // 'padding-top': '10px'
    });
    
    $('.sp-form').css({
        'padding':'0'
    });


    
    //VIDEO BOX
    
    //lucky labs
    $('.lucky-labs_player').on('click', function() {
        $('.modaLuckyLabsVideo .modal-body').append('<iframe width="560" height="315" src="https://www.youtube.com/embed/plHYxcMW_Os" frameborder="0" allowfullscreen></iframe>');
    });
    $('.close-video, .modal').on('click', function() {
        $('.modaLuckyLabsVideo .modal-body').find('iframe').remove();
    });

    //video-reviews_player
    $('.video-reviews_player').on('click', function() {
        $('.modalVideoReviews1 .modal-body').append('<iframe width="560" height="315" src="https://www.youtube.com/embed/IA4eKoVZTvU?list=PLBqDMGY_4EGF6sjCYBuC9s5REpVwcqhIz" frameborder="0" allowfullscreen></iframe>');
        $('.modalVideoReviews2 .modal-body').append('<iframe width="560" height="315" src="https://www.youtube.com/embed/SYhEr0A38cg?list=PLBqDMGY_4EGF6sjCYBuC9s5REpVwcqhIz" frameborder="0" allowfullscreen></iframe>');
        $('.modalVideoReviews3 .modal-body').append('<iframe width="560" height="315" src="https://www.youtube.com/embed/95Z_gyY2QvQ?list=PLBqDMGY_4EGF6sjCYBuC9s5REpVwcqhIz" frameborder="0" allowfullscreen></iframe>');
        $('.modalVideoReviews4 .modal-body').append('<iframe width="560" height="315" src="https://www.youtube.com/embed/7pv-o04Uyxg?list=PLBqDMGY_4EGF6sjCYBuC9s5REpVwcqhIz" frameborder="0" allowfullscreen></iframe>');
        $('.modalVideoReviews5 .modal-body').append('<iframe width="560" height="315" src="https://www.youtube.com/watch?v=t7KJc2D4s6k&feature=youtu.be" frameborder="0" allowfullscreen></iframe>');
        $('.modalVideoReviews6 .modal-body').append('<iframe width="560" height="315" src="https://youtu.be/NmH-BUBMAKI?list=PLBqDMGY_4EGF6sjCYBuC9s5REpVwcqhIz" frameborder="0" allowfullscreen></iframe>');
    });
    $('.close-video, .modal').on('click', function() {
        $('.modalVideoReviews1 .modal-body').find('iframe').remove();
        $('.modalVideoReviews2 .modal-body').find('iframe').remove();
        $('.modalVideoReviews3 .modal-body').find('iframe').remove();
        $('.modalVideoReviews4 .modal-body').find('iframe').remove();
        $('.modalVideoReviews5 .modal-body').find('iframe').remove();
        $('.modalVideoReviews6 .modal-body').find('iframe').remove();
        $('.modalVideoReviews7 .modal-body').find('iframe').remove();
        $('.modalVideoReviews8 .modal-body').find('iframe').remove();
    });

    //popular-video
    $('.play-v-box').on('click', function() {
        $('.modalVideo1 .modal-body').append('<iframe width="100%" height="315" src="https://www.youtube.com/embed/UVhoNbEM_iY" frameborder="0" allowfullscreen></iframe>');
        $('.modalVideo2 .modal-body').append('<iframe width="100%" height="315" src="https://www.youtube.com/embed/IixXMFpyPB4" frameborder="0" allowfullscreen></iframe>');
        $('.modalVideo3 .modal-body').append('<iframe width="100%" height="315" src="https://www.youtube.com/embed/CPyvvJnLN-A" frameborder="0" allowfullscreen></iframe>');
        $('.modalVideo4 .modal-body').append('<iframe width="100%" height="315" src="https://www.youtube.com/embed/d-GsUwXrKog" frameborder="0" allowfullscreen></iframe>');
        $('.modalVideo5 .modal-body').append('<iframe width="100%" height="315" src="https://www.youtube.com/embed/_f3GQk3S66g" frameborder="0" allowfullscreen></iframe>');
        $('.modalVideo6 .modal-body').append('<iframe width="100%" height="315" src="https://www.youtube.com/embed/H2LT43HAJ2o" frameborder="0" allowfullscreen></iframe>');
        $('.modalVideo7 .modal-body').append('<iframe width="100%" height="315" src="https://www.youtube.com/embed/UVhoNbEM_iY" frameborder="0" allowfullscreen></iframe>');
        $('.modalVideo8 .modal-body').append('<iframe width="100%" height="315" src="https://www.youtube.com/embed/CPyvvJnLN-A" frameborder="0" allowfullscreen></iframe>');
        
    });
    $('.close-video, .modal').on('click', function() {
        //document.location.reload();

        $('.modalVideo1 .modal-body').find('iframe').remove();
        $('.modalVideo2 .modal-body').find('iframe').remove();
        $('.modalVideo3 .modal-body').find('iframe').remove();
        $('.modalVideo4 .modal-body').find('iframe').remove();
        $('.modalVideo5 .modal-body').find('iframe').remove();
        $('.modalVideo6 .modal-body').find('iframe').remove();
        $('.modalVideo7 .modal-body').find('iframe').remove();
        $('.modalVideo8 .modal-body').find('iframe').remove();
        

        //console.log(iframe);
    });



});
