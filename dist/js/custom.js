$(window).load(function(){
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		$('body').addClass('ios');
	} else{
		$('body').addClass('web');
	};
	$('body').removeClass('loaded'); 
});
/* viewport width */
function viewport(){
	var e = window, 
		a = 'inner';
	if ( !( 'innerWidth' in window ) )
	{
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
};
/* viewport width */
$(function(){
	/* placeholder*/	   
	$('input, textarea').each(function(){
 		var placeholder = $(this).attr('placeholder');
 		$(this).focus(function(){ $(this).attr('placeholder', '');});
 		$(this).focusout(function(){			 
 			$(this).attr('placeholder', placeholder);  			
 		});
 	});
	/* placeholder*/

	$('.button-nav').click(function(){
		$(this).toggleClass('active'), 
		$('.main-nav-list').slideToggle(); 
		return false;
	});
	
	/* components */
	
	
	
	if($('.styled').length) {
		$('.styled').styler();
	};
	if($('.fancybox').length) {
		$('.fancybox').fancybox({
			margon  : 10,
			padding  : 10
		});
	};




	$('.header-slick').slick({
		 slidesToShow: 3,
		  slidesToScroll: 1,
		  autoplaySpeed: 2000,
		  arrows: false,
		  responsive: [
				{
				  breakpoint: 769,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,
					dots: false
				  }
				}				
			]
	});

	// if($('.slick-slider').length) {
	// 	$('.slick-slider').slick({
	// 		dots: true,
	// 		infinite: false,
	// 		speed: 300,
	// 		slidesToShow: 4,
	// 		slidesToScroll: 4,
	// 		responsive: [
	// 			{
	// 			  breakpoint: 1024,
	// 			  settings: {
	// 				slidesToShow: 3,
	// 				slidesToScroll: 3,
	// 				infinite: true,
	// 				dots: true
	// 			  }
	// 			},
	// 			{
	// 			  breakpoint: 600,
	// 			  settings: "unslick"
	// 			}				
	// 		]
	// 	});
	// };

	// if($('.scroll').length) {
	// 	$(".scroll").mCustomScrollbar({
	// 		axis:"x",
	// 		theme:"dark-thin",
	// 		autoExpandScrollbar:true,
	// 		advanced:{autoExpandHorizontalScroll:true}
	// 	});
	// };
	
	
	
	/* components */

	// var myMap;

	// ymaps.ready(init);

	// function init () {
	//     myMap = new ymaps.Map('map', {
	//         // Санкт-Петербург
	//         center: [59.93772, 30.313622],
	//         zoom: 10
	//     }, {
	//         searchControlProvider: 'yandex#search'
	//     });

	//     myMap.behaviors
	//         // Отключаем часть включенных по умолчанию поведений:
	//         //  - drag - перемещение карты при нажатой левой кнопки мыши;
	//         //  - magnifier.rightButton - увеличение области, выделенной правой кнопкой мыши.
	//         .disable(['drag', 'rightMouseButtonMagnifier'])
	//         // Включаем линейку.
	//         .enable('ruler');

	//     // Изменяем свойство поведения с помощью опции:
	//     // изменение масштаба колесом прокрутки будет происходить медленно,
	//     // на 1/2 уровня масштабирования в секунду.
	//     myMap.options.set('scrollZoomSpeed', 0.5);
	// }
	
	

});

var handler = function(){
	
	var height_footer = $('footer').height();	
	var height_header = $('header').height();		
	//$('.content').css({'padding-bottom':height_footer+40, 'padding-top':height_header+40});
	
	
	var viewport_wid = viewport().width;
	var viewport_height = viewport().height;
	
	if (viewport_wid <= 991) {
		
	}
	
}
$(window).bind('load', handler);
$(window).bind('resize', handler);



